## proyecto node
para realizar pruebas del api de node

# para generar json
https://mockaroo.com/


nose para que sirve
git config --global push.default simple


req.params :  para parametros que te envian en la url con dos puntos
ejemplo:
* /v2/movimientos/:id

req.query :  cuando se requiera enviar varios parametros se utiliza la interrogacion y se concatena con &

ejemplo:
* /v2/movimientosq?id=33&otrovalor=hola


#  para recuperar el json recibido en el body.
var bodyparse=require('body-parser');
app.use(bodyparse.json());

y con request.body recuperas el valor recibido en el body

![image](https://image.flaticon.com/icons/svg/1256/1256654.svg)

## DOCKER

# Para construir una imagen (el punto al final indica que quiere cargar toda la carpeta en la que se encuentra ubicada en el dockerfile)
sudo docker build -t noelfv/minode .
sudo docker build -t noelfv/minode:1.0 .

# Para listar nuestras imagenes creadas
sudo docker images -a

# Para arrancar nuestro contenedor
sudo docker run -p 2019:3000 -d  noelfv/minode
sudo docker run -p 2019:3000 -d  noelfv/minode:1.0

# Para saber si un contenedor se encuentra en ejecucion
sudo docker ps -a

# Para ingresar al entorno de ejecucion del contenedor (d154af2cf943 es el CONTAINER ID)
sudo docker exec -it d154af2cf943 /bin/bash


# enviar a docker hub una imagen
sudo docker login
sudo docker push noelfv/minode


# diferencia al saber si una imagen se encuentra en ejecucion
(ACTIVO)
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                    PORTS                    NAMES
d154af2cf943        noelfv/minode       "npm start"         15 seconds ago      Up 14 seconds            0.0.0.0:2019->3000/tcp   distracted
(INACTIVO)
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                    PORTS               NAMES
ba8854e57817        ca8f048605d6        "npm start"         21 hours ago        Exited (0) 21 hours ago                       flamboyant_ritc

# Detener un contene en especifico(d154af2cf943 CONTAINER ID)
sudo docker stop d154af2cf943

# Detener todos los contenedores
docker stop $(docker ps -a -q)

# Borrar todos los contenedores
docker rm $(docker ps -a -q)

# Borrar todas las imagenes
docker rmi $(docker images -q)





# Instalacion del packete json en node
docker rmi $(docker images -q)
