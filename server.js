//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

//los header sirven para que el json pueda ser intrpretado por el navegador
var bodyParser=require('body-parser');
app.use(bodyParser.json());
app.use(function(req,res,next){
 res.header("Access-Control-Allow-Origin","*");
 res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
 next();
});

var requestjson = require('request-json');
var path = require('path');
var urlMovimientos = "https://api.mlab.com/api/1/databases/wflores/collections/movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMlab=requestjson.createClient(urlMovimientos);

app.listen(port);





console.log('todo list RESTful API server started on: ' + port);

app.get("/",function(req,res){
  res.sendFile(path.join(__dirname,"index.html"));
})

app.post("/",function(req,res){
  res.send("envio via post");
})

app.get("/Clientes/:idcliente",function(req,res){
  res.send("aqui tienes al cliente numero: "+req.params.idcliente);
})

app.get("/api/Clientes/:idcliente",function(req,res){
  var mijson= "{'idcliente':12345}"
  res.send(mijson);
})



app.get("/v1/movimientos",function(req,res){
  res.sendFile(path.join(__dirname,"movimientosv1.json"));
})

var movimientosJSON=require('./movimientosv2.json');

app.get("/v2/movimientos",function(req,res){
  res.json(movimientosJSON);
})

app.get("/v2/movimientos/:id",function(req,res){
  console.log(req.params.id);
  res.send(movimientosJSON[req.params.id-1]);
})


app.get("/v2/movimientosq",function(req,res){
  console.log(req.query);
  res.send("recibido");
})


var bodyparse=require('body-parser');
app.use(bodyparse.json());


app.post("/v2/movimientos",function(req,res){
  var nuevo = req.body;
  nuevo.id=movimientosJSON.length+1;
  movimientosJSON.push(nuevo);
  res.send("Movimientos dado de alta");
})



//recuperar datos de mlab(mongodb)
app.get('/movimientos',function(req,res){

  clienteMlab.get('',function(err,resM,body){
    if (err){
      console.log(err);
    }else{
      res.send(body);
    }
  });
})


app.post("/movimientos",function(req,res){
  clienteMlab.post('',req.body,function(err,resM,body){
    if (err){
      console.log(err);
    }else{
      res.send(body);
    }
  });
})
